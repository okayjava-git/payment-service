package com.okayjava.payment.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.okayjava.payment.service.communication.PaymentCartCommunication;

@RestController
@RequestMapping("/payment")
public class PaymentRestController {

	@Autowired
	private PaymentCartCommunication communication;
	
	@GetMapping("/data")
	public String getData() {
		
		return "From PAYMENT-SERVICE " + communication.getCartInfo();
	}
}

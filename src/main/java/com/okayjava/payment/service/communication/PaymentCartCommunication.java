package com.okayjava.payment.service.communication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class PaymentCartCommunication {

	@Autowired
	LoadBalancerClient loadBalancerClient;
	
	public String getCartInfo() {
		
		// get ServiceInstance list using serviceId
		ServiceInstance serviceInstance = loadBalancerClient.choose("CART-SERVICE");
		
		// endpoint -  // read URI and Add path that returns url
		String uri = serviceInstance.getUri()+"/cart/data";
		// RestTemplate  create object for RestTemplate
		
		RestTemplate restTemplate = new RestTemplate();
		
		String response = restTemplate.getForObject(uri, String.class);
		
	return response;	
		
	}
}
